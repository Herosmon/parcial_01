<?php

require "persistencia/peliculaDAO.php";

class Pelicula{

    private $idPelicula;
    private $titulo;
    private $genero;
    private $duracion;



    function pelicula ($pidPelicula="", $pTitulo="", $pGenero="", $pDuracion="") {
        $this -> idPelicula = $pidPelicula;
        $this -> titulo = $pTitulo;
        $this -> genero = $pGenero;
        $this -> duracion = $pDuracion;
        $this -> conexion = new Conexion();
        $this -> peliculaDAO = new PeliculaDAO($pidPelicula, $pTitulo, $pGenero ,$pDuracion );        
    }



    public function getIdPelicula()
    {
        return $this->idPelicula;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }
    public function getGenero()
    {
        return $this->genero;
    }
    public function getDuracion()
    {
        return $this->duracion;
    }

    function crear(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> peliculaDAO -> crear());
        $this -> conexion -> cerrar();
    }


    

}